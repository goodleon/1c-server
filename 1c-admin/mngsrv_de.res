  �'      ResB             �-     C  �.  �.  �       b  IDS_SETPROP_SESSIONPARAMSETTINGS_ONLY IDS_EDITBTNTOOLTIP_CLEAR IDS_EDITBTNTOOLTIP_LISTSEL IDS_EDITBTNTOOLTIP_SPECSEL IDS_EDITBTNTOOLTIP_OPEN IDS_EDITBTNTOOLTIP_UP IDS_EDITBTNTOOLTIP_DOWN IDS_EDITBTNTOOLTIP_DROPLIST IDS_ADDIN_TITLE IDS_FSE_TITLE IDS_CPE_TITLE IDS_AGENT_TITLE IDS_CRYPTOEXTENSION IDS_HTML_COPYRIGHT IDS_HTML_RUN_MODE_DESCR IDS_HTML_EXTERN_MODULE_INSTALL_IN_PROGRESS IDS_HTML_EXTERN_MODULE_INSTALLED IDS_HTML_EXTERN_MODULE_ALREADY_INSTALLED IDS_HTML_EXTERN_MODULE_INSTALL_FAILED IDS_CONFIRM_INSTALL_IE IDS_VERIFY_PUBLISHER IDS_INSTALL_NOW_IE IDS_PRESS_CONTINUE IDS_FILE_EXT_INSTALL IDS_CRYPTO_EXT_INSTALL IDS_AGENT_EXT_INSTALL IDS_EXT_MODULE_INSTALL IDS_CHROME_EXT_INSTALL_AGREE_MSG IDS_CHROME_EXT_INSTALL_REJECT_MSG IDS_SAFARI_EXT_INSTALL_TIP_MACOS IDS_SAFARI_EXT_INSTALL_DOWNLOAD IDS_CHROME_37_EXT_INSTALL_BEGIN IDS_CHROME_37_EXT_INSTALL_FILES_EXT IDS_CHROME_37_EXT_INSTALL_CRYPTO_EXT IDS_CHROME_37_EXT_INSTALL_AGENT_EXT IDS_CHROME_37_EXT_INSTALL_ADDIN_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_CHROME_37_EXT_INSTALL_NEED_RESTART IDS_CHROME_37_EXT_INSTALL_WAIT IDS_CHROME_37_EXT_INSTALL_WAIT_LIN IDS_CHROME_37_EXT_INSTALL_RESTART IDS_EDGE_START_IN_IE11 IDS_CHROME_CLIPBOARD_EXT_NOT_INSTALLED IDS_CHROME_CLIPBOARD_EXT_NEED_RESTART IDS_CHROME_EXT_ATTEMPT_TO_USE IDS_CHROME_EXT_INSTALL_MAYBE_LATER IDS_FF_EXT_INSTALL_WEB_EXT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_FF_CLIPBOARD_EXT_NOT_INSTALLED IDS_HTML_BROWSER_SETTINGS_INFO_CONFIRMATION IDS_HTML_COLOR_CHOOSE IDS_HTML_RED IDS_HTML_GREEN IDS_HTML_BLUE IDS_HTML_BACKGOUND_COLOR_PREVIEW IDS_HTML_TEXT_COLOR_PREVIEW IDS_HTML_OPEN_CALCULATOR IDS_HTML_OPEN_CALENDAR IDS_HTML_COPY_TO_CLIPBOARD_AS_NUMBER IDS_HTML_ADD_NUMBER_TO_CLIPBOARD IDS_HTML_SUBTRACT_NUMBER_FROM_CLIPBOARD IDS_HTML_SERVICE IDS_HTML_FORM_CUSTOMIZATION IDS_HTML_TOOLBAR_CUSTOMIZE IDS_HTML_ADD_REMOVE_BUTTONS IDS_HTML_ADD_GROUP IDS_HTML_ADD_FIELDS IDS_HTML_DELETE_CURRENT_ITEM IDS_HTML_MOVE_CURRENT_ITEM_UP IDS_HTML_MOVE_CURRENT_ITEM_DOWN IDS_HTML_MARK_ALL IDS_HTML_UNMARK_ALL IDS_HTML_DELETE IDS_HTML_MOVE_UP IDS_HTML_MOVE_DOWN IDS_HTML_RESTORE_DEFAULT_SETTINGS IDS_HTML_FORM_ITEM_PROPERTIES IDS_HTML_CHOOSE_FIELDS_TO_PLACE_ON_FORM IDS_HTML_LOADING IDS_HTML_THEMES_PANEL IDS_HTML_NAVIGATION_PANEL IDS_HTML_NAVIGATION2_PANEL IDS_HTML_OPEN_HELP IDS_HTML_SHOW_ABOUT IDS_HTML_FAVORITE_LINKS IDS_HTML_FAVORITES IDS_HTML_HISTORY IDS_HTML_ENTER_NAME_AND_PASSWORD IDS_HTML_EXIT IDS_HTML_REPEAT_ENTER IDS_HTML_FILE_UPLOADING IDS_HTML_FILE IDS_HTML_UPLOADING IDS_HTML_APPLY IDS_HTML_SHOW_BUTTON IDS_HTML_HISTORY_TITLE IDS_HTML_TOPICSELECTOR_TITLE IDS_HTML_FILE_DOWNLOADING IDS_RTE_ACTIVEDOCUMENTGET_ERROR IDS_RTE_CONNECTION_UNEXPECTEDLY_CLOSED IDS_RESET_TOOLBAR IDS_HTML_CLIPSTORE IDS_HTML_CLIPPLUS IDS_HTML_CLIPMINUS IDS_HTML_WINDOW_ACTIVATED_MSG IDS_HTML_UNABLE_TO_SWITCH_TO_MAINWINDOW IDS_HTML_CLIPBOARD_INACCESSIBLE IDS_HTML_USE_BROWSER_CLIPBOARD_COMMANDS IDS_HTML_CLIPBOARD_OPERATION_DISABLED IDS_TE_TITLE IDS_TE_MESSAGE IDS_TE_INTERRUPT IDS_CMD_HELPTRAINING IDS_WEB_COMPAT_PLATFORM_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_VERSION_UNSUPPORTED IDS_WEB_COMPAT_WORK_IMPOSSIBLE IDS_WEB_BROWSERSLIST_HEADER IDS_WEB_BROWSERSLIST_DOWNLOAD IDS_WEB_BROWSERSLIST_VERSION_IE IDS_WEB_BROWSERSLIST_VERSION_FF IDS_WEB_BROWSERSLIST_VERSION_CHROME IDS_WEB_BROWSERSLIST_VERSION_SAFARI IDS_WEB_BROWSERSLIST_VERSION_SAFARI_IPAD IDS_WEB_BROWSER_UPDATE_LINK_IE IDS_WEB_BROWSER_UPDATE_LINK_SAFARI IDS_WEB_BROWSER_UPDATE_LINK_SAFARI_IPAD IDS_HTML_SET_WARN_EXT_COMP IDS_HTML_SET_GET_FILE IDS_HTML_SET_WARN_LINK_GET_FILE IDS_EXECUTE_NOT_SUPPORTED IDS_SEARCH_PROCESSING IDS_LOGOUT_WORK_FINISH IDS_WORK_IN_FULLSCREEN IDS_WORK_ONLY_IN_FULLSCREEN IDS_START_WORKING IDS_CONTINUE_WORK IDS_ECS_BROWSER_NOT_SUPPORTED IDS_ECS_MEDIA_REQUIRED_SECURE_ORIGIN IDS_ECS_MEDIA_NOT_AVAILABLE IDC_OIDC_STANDARD_LOGIN_NAME IDC_OIDC_ANOTHER_SERVICES IDC_OIDC_ERROR browsersettingsinfoieru.html browsersettingsinfoieen.html browsersettingsinfochru.html browsersettingsinfochen.html browsersettingsinfosfru.html browsersettingsinfosfen.html browsersettingsinfoff.html browsersettingsinfomain.html   R o t   B l a u   G r � n   T o o l s   L a d e n   L i z e n z   F e h l e r   L � s c h e n   S t a r t e n   S u c h e . . .   L a d e n . . .   A n z e i g e n   H i s t o r i e   A b b r e c h e n   F a v o r i t e s   E i n t r e t e n   H i n z u f � g e n   � b e r n e h m e n   V e r g r � � e r n   V e r k l e i n e r n   D a t e i   l a d e n   H i l f e   � f f n e n   A k t i o n l e i s t e   T e x t b e i s p i e l   S u b t r a h i e r e n   I n f o   a n z e i g e n   � b u n g s v e r s i o n   H e r u n t e r l a d e n   A r b e i t   b e e n d e n   A u s w � h l e n   ( F 4 )   K a p i t e l a u s w a h l   A l l e   m a r k i e r e n   R e c h n e r   � f f n e n   K a l e n d e r   � f f n e n   B e s u c h t e   S e i t e n   F a r b e   a u s w � h l e n   E r n e u t   a n m e l d e n   A u f   W i e d e r s e h e n !   A b s c h n i t t s l e i s t e   J e t z t   n e u   s t a r t e n   F e l d e r   h i n z u f � g e n   G r u p p e   h i n z u f � g e n   N a v i g a t i o n s l e i s t e   E x t e r n e   K o m p o n e n t e   A u f g a b e n a u s f � h r u n g   E r h a l t e n   d e r   D a d e i   E r h a l t e n   d e r   D a t e i   L � s c h e n   ( S h i f t + F 4 )   F o r m u l a r e i n s t e l l u n g   H i n t e r g r u n d b e i s p i e l   A l s   N u m m e r   k o p i e r e n   D U R C H   A N D E R E   D I E N S T E   A n d e r e   S c h a l t f l � c h e n   I n s t a l l a t i o n   s t a r t e n   N a c h   o b e n   v e r s c h i e b e n   N a c h   u n t e n   v e r s c h i e b e n   � f f n e n   ( C t r l + S h i f t + F 4 )   A u s   d e r   L i s t e   a u s w � h l e n   A r b e i t   i m   V o l l b i l d m o d u s   I n s t a l l a t i o n   v e r s c h i e b e n   K o m p o n e n t e   h e r u n t e r l a d e n   " I n s t a l l i e r e n   /   I n s t a l l "   M i t   d e r   A r b e i t   f o r t f a h r e n   A k t u e l l e s   E l e m e n t   l � s c h e n   V i d e o k a m e r a   n i c h t   v e r f � g b a r   h t t p : / / w w w . a p p l e . c o m / r u / i o s /   Z u r � c k s e t z e n   d e r   B e f e h l s l e i s t e   Z a h l   i n   Z w i s c h e n a b l a g e   k o p i e r e n   S t a n d a r d e i n s t e l l u n g e n   s e t z e n . . .   D e r   V o r g a n g   i s t   n i c h t   v e r f � g b a r .   L i s t e   d e r   u n t e r s t � t z t e n   B r o w s e r s   W i e   r i c h t e t   m a n   d e n   B r o w s e r   e i n ?   Z a h l   a u s   Z w i s c h e n a b l a g e   e n t f e r n e n   E i g e n s c h a f t e n   d e s   F o r m u l a r e l e m e n t s   Z a h l   z u r   Z w i s c h e n a b l a g e   h i n z u f � g e n   A r b e i t   n u r   i m   V o l l b i l d m o d u s   m � g l i c h   M � c h t e n   S i e   e i n e n   N e u s t a r t   a u s f � h r e n ?   M a r k i e r u n g   f � r   a l l e   E l e m e n t e   a u f h e b e n   " 1 C :   E n t e r p r i s e   -   A l e r t s   u n d   S t a r t - U p "   E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   D a t e i e n   W e c h s e l   z u m   H a u p t f e n s t e r   n i c h t   m � g l i c h .   I n s t a l l i e r e n   e i n e r   e x t e r n e n   K o m p o n e n t e .   S c h a l t f l � c h e n   h i n z u f � g e n   o d e r   e n t f e r n e n   A k t u e l l e s   E l e m e n t   n a c h   o b e n   v e r s c h i e b e n   D i e   e x t e r n e   K o m p o n e n t e   w i r d   i n s t a l l i e r t   h t t p : / / w w w . a p p l e . c o m / r u / s a f a r i / d o w n l o a d /   A k t u e l l e s   E l e m e n t   n a c h   u n t e n   v e r s c h i e b e n   +�D i e   e x t e r n e   K o m p o n e n t e   w i r d   i n s t a l l i e r t   . . .   ,�E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   K r y p t o g r a p h i e   -�W � h l e n   S i e   F e l d e r   z u m   E i n g l i e d e r n   i n   F o r m u l a r   /�A u s   d e r   L i s t e   a u s w � h l e n   ( S t r g + N a c h - u n t e n - T a s t e )   0�I h r   B r o w s e r   w i r d   v o n   1 !: D i a l o g   n i c h t   u n t e r s t � t z t   3�H i n t e r g r u n d j o b s   w e r d e n   b e a r b e i t e t .  
 B i t t e   w a r t e n . . .   4�D i e   E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   d e r   K r y p t o g r a p h i e   4ܩ     " 1 !- S o f t " ,   1 9 9 6 - 2 0 1 8 .   A l l e   R e c h t e   v o r b e h a l t e n .   5�D i e   e x t e r n e   K o m p o n e n t e   w u r d e   e r f o l g r e i c h   i n s t a l l i e r t .   8�I n s t a l l i e r e n   d e r   E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   D a t e i e n .   ;�D i e   A u s f � h r e n - A n w e i s u n g   w i r d   i m   W e b c l i e n t   n i c h t   u n t e r s t � t z t   ;�D i e   E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   D a t e i e n   w i r d   i n s t a l l i e r t   =�U m   d i e   I n s t a l l a t i o n   z u   s t a r t e n ,   k l i c k e n   S i e   a u f   " F o r t f a h r e n " .   =�G e b e n   S i e   d e n   N a m e n   u n d   B e n u t z e r k e n n w o r t   v o n   1 !: E n t e r p r i s e   a n   ?�C O M - O b j e k t e   w e r d e n   n u r   b e i   W i n d o w s - B e t r i e b s s y s t e m e n   u n t e r s t � t z t   A�E s   w u r d e   z u m   F e n s t e r   g e w e c h s e l t .   K l i c k e n   S i e   ,   u m   f o r t z u s e t z e n .   A�D i e   E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   K r y p t o g r a p h i e   w i r d   i n s t a l l i e r t   B�U n t e r s t � t z t e V e r s i o n e n :   5 2   u n d h � h e r . < b r / > E m p f o h l e n e V e r s i o n :   l e t z t e .   B�I n s t a l l i e r e n   d e r   E r w e i t e r u n g   f � r   d i e   A r b e i t   m i t   d e r   K r y p t o g r a p h i e .   B�D e r   Z u g r i f f   z u r   Z w i s c h e n a b l a g e   i s t   i n   I h r e m   B r o w s e r   n i c h t   e r l a u b t .   B�I n s t a l l a t i o n   d e s   P r o g r a m m s   " 1 C :   E n t e r p r i s e   -   A l e r t s   a n d   S t a r t - U p " .   E�U n t e r s t � t z t e   V e r s i o n e n :   1 0   u n d   h � h e r . < b r / > E m p f o h l e n e   V e r s i o n :   l e t z t e .   E�h t t p : / / w i n d o w s . m i c r o s o f t . c o m / r u - R U / i n t e r n e t - e x p l o r e r / p r o d u c t s / i e / h o m e   F�U n t e r s t � t z t e   V e r s i o n e n :   4 . 0   u n d   h � h e r . < b r / > E m p f o h l e n e   V e r s i o n :   l e t z t e .   F�E s   i s t   e i n e   v o r � b e r g e h e n d e   S t � r u n g   d e r   I n t e r n e t v e r b i n d u n g   a u f g e t r e t e n .   G�U n t e r s t � t z t e V e r s i o n e n :   4 . 0 . 5   u n d   h � h e r . < b r / > E m p f o h l e n e   V e r s i o n :   l e t z t e .   J�W e n n   k e i n e   D a t e i   e m p f a n b e n   w e r d e n   k o n n t e ,   f � h r e n   S i e   d i e   E i n r i c h t u n g   d u r c h   K�D i e   I n s t a l l a t i o n   d e s   P r o g r a m m s   " 1 C :   E n t e r p r i s e   -   A l e r t s   u n d   S t a r t - U p "   l � u f t   P�F � r   d e n   Z u g r i f f   a u f   V i d e o k a m e r a   i s t   e i n e   s i c h e r e   V e r b i n d u n g   e r f o r d e r l i c h   ( h t t p s )   Q�U n t e r s t � t z t e V e r s i o n e n :   4 . 0 . 4   ( i O S   3 . 2 )   u n d   h � h e r . < b r / > E m p f o h l e n e   V e r s i o n :   l e t z t e .   Q�D i e   e x t e r n e   K o m p o n e n t e   i s t   e r f o l g r e i c h   i n s t a l l i e r t   o d e r   w u r d e   f r � h e r   i n s t a l l i e r t .   R�B e n u t z e n   S i e   S t r g + C   z u m   K o p i e r e n ,   S t r g + X   z u m   A u s s c h n e i d e n   u n d   S t r g + V   z u m   E i n f � g e n .   c�D i e   E i g e n s c h a f t   k a n n   n a c h   d e m   A u f r u f e n   d e r   E i n s t e l l u n g   v o n   S i t z u n g s p a r a m e t e r n   n i c h t   g e � n d e r t   w e r d e n   i�I n s t a l l a t i o n   d e r   e x t e r n e n   K o m p o n e n t e   f e h l g e s c h l a g e n ! 
 W � h r e n d   d e r   I n s t a l l a t i o n   i s t   e i n   F e h l e r   a u f g e t r e t e n !   j�< p > U m   % s   z u   i n s t a l l i e r e n ,   m a c h e n   S i e   d i e   e m p f a n g e n e   D a t e i   a u s f � h r b a r   u n d   s t a r t e n   S i e   d e r e n   A u s f � h r u n g . < / p >   n�U m   d i e   I n s t a l l a t i o n   a b z u b r e c h e n ,   k l i c k e n   S i e   a u f   " � b e r g a b e   a b b r e c h e n   /   D i s c a r d "   u n d   s c h l i e � e n   S i e   d a s   F e n s t e r .   u�W e n n   d i e   A b f r a g e   f � r   d i e   I n s t a l l a t i o n s z u l a s s u n g   e r s c h e i n t ,   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " I n s t a l l i e r e n   /   I n s t a l l " .   z�< p > U m   % s   z u   i n s t a l l i e r e n ,   k l i c k e n   S i e   a u f   d i e   e m p f a n g e n e   D a t e i   u n d   w a r t e n   S i e ,   b i s   d i e   I n s t a l l a t i o n   a b g e s c h l o s s e n   i s t . < / p >   ��I h r e   B r o w s e r v e r s i o n   w i r d   v o n   1 !: E n t e r p r i s e   n i c h t   u n t e r s t � t z t . 
 K l i c k e n   S i e   a u f   O K ,   u m   d i e   L i s t e   d e r   u n t e r s t � t z t e n   B r o w s e r s   a n z u z e i g e n .   ��N a c h   d e r   e r f o l g r e i c h e n   I n s t a l l a t i o n   d e r   E r w e i t e r u n g   f � r   d e n   B r o w s e r   m � s s e n   S i e   d i e   S e i t e   n e u   l a d e n . 
 K l i c k e n   S i e   a u f   " J e t z t   n e u   s t a r t e n "   z u m   N e u l a d e n .   ��U m   d i e   I n s t a l l a t i o n   z u   s t a r t e n ,   k l i c k e n   S i e   a u f   " W e i t e r /   C o n t i n u e " .   D a n n   b e s t � t i g e n   S i e   d i e   I n s t a l l a t i o n ,   i n d e m   S i e   a u f   d i e   S c h a l t f l � c h e   " I n s t a l l i e r e n   /   I n s t a l l "   k l i c k e n .   ��< p > D i e   B r o w s e r e r w e i t e r u n g   w u r d e   i n s t a l l i e r t .   J e t z t   m u s s   n o c h   % s   i n s t a l l i e r t   w e r d e n . < / p > < p > D a f � r   m a c h e n   S i e   d i e   e m p f a n g e n e   D a t e i   a u s f � h r b a r   u n d   s t a r t e n   S i e   d e r e n   A u s f � h r u n g . < / p > < p >   ��< p > N a c h   d e r   e r f o l g r e i c h e n   I n s t a l l a t i o n   d e r   E r w e i t e r u n g   f � r   d e n   B r o w s e r   u n d   d e r   K o m p o n e n t e   m � s s e n   S i e   d i e   S e i t e   n e u   l a d e n .   K l i c k e n   S i e   a u f   < b > " J e t z t   n e u   s t a r t e n " < b >   z u m   N e u l a d e n . < / p >   ��D i e   W e b s i t e   % h o s t n a m e %   v e r s u c h t   a u f   d i e   Z w i s c h e n a b l a g e   z u z u g r e i f e n .   K l i c k e n   S i e   a u f   ' O K ' ,   u m   d e n   Z u g r i f f   a u f   d i e s e   W e b s i t e   z u l a s s e n .   K l i c k e n   S i e   a u f   ' A b b r e c h e n ' ,   u m   d e n   Z u g r i f f   z u   v e r w e i g e r n .   ��I h r   B r o w s e r   w i r d   v o n   1 C : E n t e r p r i s e   n i c h t   u n t e r s t � t z t . 
 S y s t e m b e t r i e b   k a n n   f e h l e r h a f t   s e i n . 
 W � h l e n   S i e   O K ,   u m   f o r t z u f a h r e n . 
 W � h l e n   S i e   A b b r e c h e n   ( C a n c e l ) ,   u m   d i e   L i s t e   d e r   u n t e r s t � t z t e n   B r o w s e r s   a n z u z e i g e n .   ��D i e   V e r s i o n   I h r e s   B r o w s e r s   w i r d   v o n   1 C : E n t e r p r i s e   n i c h t   u n t e r s t � t z t . 
 S y s t e m b e t r i e b   k a n n   f e h l e r h a f t   s e i n . 
 W � h l e n   S i e   O K ,   u m   f o r t z u f a h r e n . 
 W � h l e n   S i e   A b b r e c h e n   ( C a n c e l ) ,   u m   d i e   L i s t e   d e r   u n t e r s t � t z t e n   B r o w s e r s   a n z u z e i g e n .   ��I h r   B e t r i e b s s y s t e m   w i r d   v o n   1 C : E n t e r p r i s e   n i c h t   u n t e r s t � t z t . 
 S y s t e m b e t r i e b   k a n n   f e h l e r h a f t   s e i n . 
 W � h l e n   S i e   O K ,   u m   f o r t z u f a h r e n . 
 W � h l e n   S i e   A b b r e c h e n   ( C a n c e l ) ,   u m   d i e   L i s t e   d e r   u n t e r s t � t z t e n   B e t r i e b s s y s t e m e   u n d   B r o w s e r s   a n z u z e i g e n .   ��U m   a u f   d i e s e   F u n k t i o n a l i t � t   z u z u g r e i f e n ,   e m p f e h l e n   w i r   d i e   A p p l i k a t i o n   i m   I n t e r n e t   E x p l o r e r - B r o w s e r   z u   s t a r t e n . 
 D a f � r   � f f n e n   S i e   d a s   B r o w s e r - M e n �   m i t   d e r   S c h a l t f l � c h e   " . . . "   u n d   w � h l e n   S i e   d e n   P u n k t   " I m   I n t e r n e t   E x p l o r e r   � f f n e n "   a u s .   ��< p > D i e   B r o w s e r e r w e i t e r u n g   w u r d e   i n s t a l l i e r t . S i e   m � s s e n   n u r   n o c h   % s   i n s t a l l i e r e n . < / p > < p > D a z u   k l i c k e n   S i e   a u f   d i e   e m p f a n g e n e   D a t e i   ( i m   u n t e r e n   B e r e i c h   d e s   B r o w s e r f e n s t e r s )   u n d   w a r t e n   S i e ,   b i s   d i e   I n s t a l l a t i o n   a b g e s c h l o s s e n   i s t . < / p > < p >   �W e n n   d i e   I n s t a l l a t i o n s a b f r a g e   e r s c h e i n t ,   s t e l l e n   S i e   s i c h e r ,   d a s s   d e r   z u v e r l � s s i g e   L i e f e r a n t   d e r   K o m p o n e n t e   a l s   V e r f a s s e r   a n g e g e b e n   i s t !   U m   d i e   I n s t a l l a t i o n   a b z u b r e c h e n ,   k l i c k e n   S i e   a u f   " A b b r e c h e n   /   C a n c e l " .   U m   d i e   I n s t a l l a t i o n   z u   b e s t � t i g e n ,   k l i c k e n   S i e   a u f   �< p > D i e   B r o w s e r e r w e i t e r u n g   w u r d e   i n s t a l l i e r t .   J e t z t   m u s s   n o c h   % s   i n s t a l l i e r t   w e r d e n . < / p > < p > K l i c k e n   S i e   a u f   < b > " F o r t f a h r e n " < / b >   u m   d i e   I n s t a l l a t i o n   z u   s t a r t e n . < / p > < p > D i e   K o m p o n e n t e   w i r d   h e r u n t e r g e l a d e n .   D a n a c h   m a c h e n   S i e   d i e   e m p f a n g e n e   D a t e i   a u s f � h r b a r   u n d   s t a r t e n   S i e   d e r e n   A u s f � h r u n g . < p >   )�< p > D a m i t   d i e   A k t i o n   a u s g e f � h r t   w e r d e n   k a n n ,   m u s s   % s   i n s t a l l i e r t   s e i n . < / p > < p > K l i c k e n   S i e   a u f   < b > " F o r t f a h r e n " < / b > ,   u m   d i e   I n s t a l l a t i o n   z u   s t a r t e n . < / p > < p > D i e   K o m p o n e n t e   w i r d   h e r u n t e r g e l a d e n .   A n s c h l i e � e n d   k l i c k e n   S i e   a u f   d i e   e m p f a n g e n e   D a t e i   u n d   w a r t e n   S i e ,   b i s   d i e   I n s t a l l a t i o n   a b g e s c h l o s s e n   i s t . < / p >   1�< p > D a m i t   d i e   A k t i o n   a u s g e f � h r t   w e r d e n   k a n n ,   m u s s   d i e   E r w e i t e r u n g   d e s   B r o w s e r s   u n d   % s   i n s t a l l i e r t   s e i n . < / p > < p > K l i c k e n   S i e   a u f   < b > " F o r t f a h r e n " < / b >   u m   d i e   I n s t a l l a t i o n   z u   s t a r t e n . < / p > < p > D i e   K o m p o n e n t e   w i r d   h e r u n t e r g e l a d e n .   D a n n   m a c h e n   S i e   d i e   e m p f a n g e n e   D a t e i   a u s f � h r b a r   u n d   s t a r t e n   S i e   d e r e n   A u s f � h r u n g . < / p >   4�< p > D i e   B r o w s e r e r w e i t e r u n g   w u r d e   i n s t a l l i e r t .   S i e   m � s s e n   n u r   n o c h   % s   i n s t a l l i e r e n . < / p > < p > K l i c k e n   S i e   a u f   < b > " F o r t f a h r e n " < / b > ,   u m   d i e   I n s t a l l a t i o n   z u   s t a r t e n . < / p > < p > D i e   K o m p o n e n t e   w i r d   h e r u n t e r g e l a d e n .   A n s c h l i e � e n d   k l i c k e n   S i e   a u f   d i e   e m p f a n g e n e   D a t e i   u n d   w a r t e n   S i e ,   b i s   d i e   I n s t a l l a t i o n   a b g e s c h l o s s e n   i s t . < p >   ��U m   d i e   Z w i s c h e n a b l a g e   i m   B r o w s e r   F i r e f o x   i n   v o l l e m   A u s m a �   v e r w e n d e n   z u   k � n n e n ,   m u s s   d i e   E r w e i t e r u n g   f � r   d e n   B r o w s e r   i n s t a l l i e r t   w e r d e n . 
 U m   d i e   E r w e i t e r u n g   z u   i n s t a l l i e r e n ,   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " I n s t a l l a t i o n   s t a r t e n " .   W e n n   d i e   N a c h r i c h t   e r s c h e i n t ,   d a s s   d i e   I n s t a l l a t i o n s a b f r a g e   d u r c h   F i r e f o x   g e s p e r r t   w u r d e ,   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " Z u l a s s e n "   u n d   d a n n   a u f   " I n s t a l l i e r e n "   ��< p > D a m i t   d i e   A k t i o n   a u s g e f � h r t   w e r d e n   k a n n ,   m u s s   d i e   E r w e i t e r u n g   f � r   d e n   B r o w s e r   u n d   % s   i n s t a l l i e r t   s e i n . < / p > < p > K l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   < b > " F o r t f a h r e n " < / b > . < / p > < p > W e n n   d i e   N a c h r i c h t   e r s c h e i n t ,   d a s s   d i e   I n s t a l l a t i o n s a b f r a g e   d u r c h   F i r e f o x   g e s p e r r t   w u r d e ,   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " Z u l a s s e n    u n d   d a n n   a u f   " I n s t a l l i e r e n " .   B e i   A n z e i g e   d e r   I n s t a l l a t i o n s a b f r a g e   s t e l l e n   S i e   s i c h e r ,   d a s s   e i n   L i e f e r a n t   d e r   B r o w s e r e r w e i t e r u n g ,   d e m   S i e   v e r t r a u e n ,   a l s   A u t o r   a n g e g e b e n   i s t ! < / p >   �K l i c k e n   S i e   a u f   d e n   L i n k   u n t e n . 
 D e r   B r o w s e r   w i r d   d i e   I n s t a l l a t i o n s d a t e i   m i t   d e r   K o m p o n e n t e   h e r u n t e r l a d e n . 
 W e n n   d i e   D a t e i   n i c h t   a u t o m a t i s c h   g e � f f n e t   w i r d ,   � f f n e n   S i e   d i e s e   m a n u e l l ,   i n d e m   S i e   d a r a u f   i m   L o a d - F e n s t e r   d o p p e l t   k l i c k e n . 
 B e f o l g e n   S i e   d i e   a n g e z e i g t e n   A n w e i s u n g e n   u n d   f � h r e n   S i e   d i e   I n s t a l l a t i o n   a u s . 
 W � h r e n d   d e r   I n s t a l l a t i o n   k � n n e n   S i e   d e n   K o m p o n e n t e n l i e f e r a n t e n   p r � f e n ,   i n d e m   S i e   a u f   d a s   S y m b o l   r e c h t s   o b e n   i m   I n s t a l l a t i o n s f e n s t e r   k l i c k e n .   P r � f e n   S i e ,   d a s s   d e r   v o n   I h n e n   v e r t r a u t e   L i e f e r a n t   a n g e g e b e n   i s t !   :�< p > D a m i t   d i e   A k t i o n   a u s g e f � h r t   w e r d e n   k a n n ,   m u s s   d i e   E r w e i t e r u n g   f � r   d e n   B r o w s e r   u n d   % s   i n s t a l l i e r t   s e i n . < / p > < p > K l i c k e n   S i e   a u f   < b > " F o r t f a h r e n " < / b >   u m   d i e   I n s t a l l a t i o n   z u   s t a r t e n . < / p > < p > D a s   P r o g r a m m   � f f n e t   d i e   S e i t e   d e r   E r w e i t e r u n g   i m   G o o g l e   C h r o m e   W e b s t o r e .   K l i c k e n   S i e   a u f   < b > " + & n b s p ; K O S T E N L O S " < / b > ,   u m   d i e   E r w e i t e r u n g   z u   i n s t a l l i e r e n .   < / p > < p >   N a c h   d e r   I n s t a l l a t i o n   d e r   E r w e i t e r u n g   m � s s e n   S i e   d i e   S e i t e   d e r   E r w e i t e r u n g   s c h l i e � e n   u n d   d i e   e x t e r n e   K o m p o n e n t e   s e l b s t   i n s t a l l i e r e n . < / p > < p > D a z u   m a c h e n   S i e   d i e   e m p f a n g e n e   D a t e i   a u s f � h r b a r   u n d   s t a r t e n   S i e   d e r e n   A u s f � h r u n g . < / p >   ?�U m   d i e   Z w i s c h e n a b l a g e   i m   B r o w s e r   C h r o m e   i n   v o l l e m   A u s m a �   v e r w e n d e n   z u   k � n n e n ,   m u s s   d i e   E r w e i t e r u n g   f � r   d e n   B r o w s e r   i n s t a l l i e r t   w e r d e n . 
 U m   d i e   E r w e i t e r u n g   z u   i n s t a l l i e r e n ,   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " I n s t a l l a t i o n   s t a r t e n " ,   d a n n   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " + K o s t e n l o s "   a u f   d e r   g e � f f n e t e n   S e i t e   ( N e u s t a r t   d e r   A n w e n d u n g   i s t   e r f o r d e r l i c h ) . 
 D a m i t   d i e   E r w e i t e r u n g   a u t o m a t i s c h   b e i m   n � c h s t e n   S t a r t   d e r   A n w e n d u n g   i n s t a l l i e r t   w i r d ,   k l i c k e n   S i e   a u f   d i e   S c h a l t f l � c h e   " I n s t a l l a t i o n   v e r s c h i e b e n " . 
 K l i c k e n   S i e   a u f   " A b b r e c h e n " ,   d a m i t   d i e   E r w e i t e r u n g   n i c h t   s o f o r t   i n s t a l l i e r t   w i r d .   o�< p > D a m i t   d i e   A k t i o n   a u s g e f � h r t   w e r d e n   k a n n ,   m u s s   d i e   E r w e i t e r u n g   f � r   d e n   B r o w s e r   u n d   % s   i n s t a l l i e r t   s e i n . < / p > < p > K l i c k e n   S i e   a u f   < b > " F o r t f a h r e n " < / b >   u m   d i e   I n s t a l l a t i o n   z u   s t a r t e n . < / p > < p > D a s   P r o g r a m m   � f f n e t   d i e   S e i t e   d e r   E r w e i t e r u n g   i m   G o o g l e   C h r o m e   W e b s t o r e .   K l i c k e n   S i e   a u f   < b > " + & n b s p ; K O S T E N L O S " < / b > ,   u m   d i e   E r w e i t e r u n g   z u   i n s t a l l i e r e n .   < / p > < p > N a c h   d e r   I n s t a l l a t i o n   d e r   E r w e i t e r u n g   m � s s e n   S i e   d i e   S e i t e   d e r   E r w e i t e r u n g   s c h l i e � e n   u n d   d i e   e i g e n t l i c h e   K o m p o n e n t e   i n s t a l l i e r e n . < / p > < p > D a z u   k l i c k e n   S i e   a u f   d i e   e m p f a n g e n e   D a t e i   ( i m   u n t e r e n   B e r e i c h   d e s   B r o w s e r f e n s t e r s )   u n d   w a r t e n   S i e ,   b i s   d i e   I n s t a l l a t i o n   a b g e s c h l o s s e n   i s t . < / p >   �����  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>
<p class="title step">Schritt 1</p>

<p>Klicken Sie auf das Symbol <b><i>Extras</i></b> rechts oben im Fenster.</p>
<p>Im Men&uuml; w&auml;hlen Sie den Eintrag <b><i>Settings</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_1_en.png?sysver=<%V8VER%>" /></div>

<p>Daraufhin wird die Registerkarte <b><i>Settings-Basics</i></b> ge&ouml;ffnet, in der alle weiteren Einstellungen vorgenommen werden.</p> 
<p>Um zur Anleitung zur&uuml;ckzukehren, w&auml;hlen Sie die Registerkarte <b><i>Browser einrichten</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_0_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Schritt 2</p>

<p>Gehen Sie zum Link <b><i>Show advanced settings...</i></b> im unteren Fensterbereich und klicken Sie darauf.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_2_en.png?sysver=<%V8VER%>" /></div>

<p>Gehen Sie zum Bereich <b><i>Downloads</i></b> und setzen Sie ein H&auml;kchen bei <b><i>Ask where to save each file before downloading</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_3_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Schritt 3</p>

<p>Nachdem Sie das H&auml;kchen gesetzt haben, klicken Sie auf die Schaltfl&auml;che <b><i>Content Settings</i></b> oben im Bereich <b><i>Privacy</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_4_en.png?sysver=<%V8VER%>" /></div>

<p>In dem erschienenen Fenster <b><i>Content Settings</i></b> gehen Sie zum Bereich <b><i>Pop-Ups</i></b> und aktivieren Sie die Option <b><i>Allow all sites to show pop-ups</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_5_en.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">Beachten Sie, dass es bei Ihrem Browser nicht erforderlich ist, &Auml;nderungen zu speichern. Um die Einrichtung fertig zu stellen, gen&uuml;gt es, den Tab <i>Settings</i> zu schlie&szlig;en.</p>

</div>
</div>

</body>
</html>
���������������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>
<p class="title step">Schritt 1</p>

<p>Klicken Sie auf das Symbol <b><i>Extras</i></b> rechts oben im Fenster.</p>
<p>Im Men&uuml; w&auml;hlen Sie den Eintrag <b><i>Einstellungen</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Daraufhin wird die Registerkarte <b><i>Einstellungen-Standard</i></b> ge&ouml;ffnet, in der alle weiteren Einstellungen vorgenommen werden.</p> 
<p>Um zur Anleitung zur&uuml;ckzukehren, w&auml;hlen Sie die Registerkarte <b><i>Browser einrichten</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_0_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Schritt 2</p>

<p>Gehen Sie zum Link <b><i>Erweiterte Einstellungen anzeigen...</i></b> im unteren Fensterbereich und klicken Sie darauf.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_2_ru.png?sysver=<%V8VER%>" /></div>

<p>Gehen Sie zum Bereich <b><i>Downloads</i></b> und setzen Sie ein H&auml;kchen bei <b><i>Vor dem Download von Dateien nach dem Speicherort fragen</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_3_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Schritt 3</p>

<p>Nachdem Sie das H&auml;kchen gesetzt haben, klicken Sie auf die Schaltfl&auml;che <b><i>Inhaltseinstellungen</i></b> oben im Bereich <b><i>Datenschutz</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_4_ru.png?sysver=<%V8VER%>" /></div>

<p>In dem erschienenen Fenster <b><i>Inhaltseinstellungen</i></b> gehen Sie zum Bereich <b><i>Pop-Ups</i></b> und aktivieren Sie die Option <b><i>Anzeige von Pop-Ups f&uuml;r alle Websites zulassen</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_5_ru.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">Beachten Sie, dass es bei Ihrem Browser nicht erforderlich ist, &Auml;nderungen zu speichern. Um die Einrichtung fertig zu stellen, gen&uuml;gt es, den Tab <i>Einstellungen</i> zu schlie&szlig;en.</p>

</div>
</div>

</body>
</html>
��������P  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>

<ul>
    <li>Um die Pop-up-Fenster in der Anwendung zu erlauben, w&auml;hlen Sie im Men&uuml; <b>Extras (Tools)</b> des Browsers den Eintrag <b>Einstellungen (Options)</b> aus;<br> Im ge&ouml;ffneten Fenster gehen Sie zum Abschnitt <b>Inhalt (Content)</b>;<br> Deaktivieren Sie das Kontrollk&auml;stchen <b>Pop-up-Fenster blockieren (Block pop-up windows)</b>.</li>
    <li>Um das Wechseln zwischen den Fenstern der Anwendung manuell zu erlauben, geben Sie in der Adresszeile des Browsers <b>about:config</b> ein;<br> Dann tippen Sie in der Filterzeile <code>dom.disable_window_flip</code> ein;<br> &Auml;ndern Sie den Wert dieser Einstellung auf <code>false</code>.</li>
    <li>Um die Verwendung von nicht lateinischen Symbolen in den Parametern der Startzeile manuell zu erlauben, geben Sie in der Adresszeile des Browsers <b>about:config</b> ein;<br> Dann tippen Sie in der Filterzeile <code>network.standard-url.encode-query-utf8</code> ein. Wird diese Einstellung nicht gefunden, tippen Sie <code>browser.fixup.use-utf8</code>;<br> &Auml;ndern Sie den Wert dieser Einstellung auf <code>true</code>.</li>
    <li>Um das Wechseln zwischen den Fenstern der Anwendung manuell zu erlauben, geben Sie in der Adresszeile des Browsers <b>about:config</b> ein;<br>  Dann tippen Sie in der Filterzeile <code>dom.popup_allowed_events</code>;<br> F&uuml;gen Sie dem Wert dieser Einstellung das Ereignis<b>keydown</b> hinzu.</li>
    <li>Um die Authentifizierung des Betriebssystems manuell einzustellen, geben Sie in der Adresszeile des Browsers <b>about:config</b> ein;<br> Danach auf der Seite der Einstellungen geben Sie in der Filterzeile den Parameternamen ein;<br> Diese Einstellung wird f&uuml;r drei Parameter vorgenommen: <code>network.automatic-ntlm-auth.trusted-uris</code>, <code>network.negotiate-auth.delegation-uris</code>, <code>network.negotiate-auth.trusted-uris</code>;<br> Als N&auml;chstes definieren Sie eine Liste mit Webservern, &uuml;ber die die Arbeit mit der 1C:Enterprise-Basis erfolgen soll.</li>
</ul>

<p>Die Einstellung ist somit abgeschlossen.</p>

</div>
</div>

</body>
</html>
�������������
  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>
<p class="title step">Schritt 1</p>

<p>Klicken Sie mit der rechten Maustaste im beliebigen freien Bereich unter der Adresszeile (auf dem Bild mit blassem Rot markiert) und w&auml;hlen Sie in dem erschienenem Men&uuml; den Eintrag <b><i>Menu Bar</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_0_en.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="e1csys/mngsrv/img_ie_01_en.png?sysver=<%V8VER%>" /></div>

<p>Unter der Adresszeile erscheint daraufhin ein Men&uuml;. Finden Sie den Eintrag <b><i>Tools</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_1_en.png?sysver=<%V8VER%>" /></div>

<p>Klicken Sie darauf, es wird das Men&uuml; ge&ouml;ffnet. W&auml;hlen sie den Eintrag <b><i>Internet Options</i></b> aus.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_2_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Schritt 2</p>

<p>Im ge&ouml;ffneten Fenster gehen Sie zum Registerreiter <b><i>Privacy</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_3_en.png?sysver=<%V8VER%>" /></div>

<p>Im unteren Fensterbereich finden Sie und <b><i>entfernen</i></b> Sie das H&auml;kchen <b><i>Turn on Pop-up Blocker</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_4_en.png?sysver=<%V8VER%>" /></div>

<p>Nachdem das H&auml;kchen entfernt wurde, klicken Sie auf die Schaltfl&auml;che <b><i>OK</i></b>, um die &Auml;nderungen zu speichern.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_5_en.png?sysver=<%V8VER%>" /></div>

<p>Die Einstellung ist somit abgeschlossen.</p>

<p style='font-size:9pt'>Um das Men&uuml; unter der Adresszeile auszublenden, wiederholen Sie den ersten Teil des Schritts 1.</p>

</div>
</div>

</body>
</html>
����������������
  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>
<p class="title step">Schritt 1</p>

<p>Klicken Sie mit der rechten Maustaste im beliebigen freien Bereich unter der Adresszeile (auf dem Bild mit blassem Rot markiert) und w&auml;hlen Sie in dem erschienenem Men&uuml; den Eintrag <b><i>Men&uuml;leiste</i></b> aus.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_0_ru.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="e1csys/mngsrv/img_ie_01_ru.png?sysver=<%V8VER%>" /></div>

<p>Unter der Adresszeile erscheint daraufhin ein Men&uuml;. Finden Sie den Eintrag <b><i>Extras</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Klicken Sie darauf, es wird das Men&uuml; ge&ouml;ffnet. W&auml;hlen sie den Eintrag <b><i>Internetoptionen</i></b> aus.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_2_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Schritt 2</p>

<p>Im ge&ouml;ffneten Fenster gehen Sie zum Registerreiter <b><i>Datenschutz</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_3_ru.png?sysver=<%V8VER%>" /></div>

<p>Im unteren Fensterbereich finden Sie und <b><i>entfernen</i></b> Sie das H&auml;kchen <b><i>Popupblocker einschalten</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_4_ru.png?sysver=<%V8VER%>" /></div>

<p>Nachdem das H&auml;kchen entfernt wurde, klicken Sie auf die Schaltfl&auml;che <b><i>OK</i></b>, um die &Auml;nderungen zu speichern.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_5_ru.png?sysver=<%V8VER%>" /></div>

<p>Die Einstellung ist somit abgeschlossen.</p>

<p style='font-size:9pt'>Um das Men&uuml; unter der Adresszeile auszublenden, wiederholen Sie den ersten Teil des Schritts 1.</p>

</div>
</div>

</body>
</html>
��������������	  ﻿<!DOCTYPE html>
<html class="IWeb" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Browser setup</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="imagetoolbar" content="no">
        <link href="<%CSS_FILE%>?sysver=<%V8VER%>" rel="stylesheet" type="text/css">
        <style type="text/css">
        .main
        {
            width:720px;
            margin:0 auto;
        }
        .head
        {
            font-size:32pt;
            font-family:Times New Roman;
        }
        .title
        {
            font-size:20pt;
            font-family:Times New Roman;
        }
        .txt
        {
            font-size:14pt;
            font-family:Times New Roman;
        }
        </style>
</head>
<body style="width:100%;height:100%;overflow:hidden;padding:0;margin:0;">
<div style="width:100%;height:100%;overflow-y:auto;overflow-x:hidden;">
<div style="padding:10px;padding-left:30px;padding-right:30px;">
<p><span class="txt"><b>A new window has been blocked, presumably by a pop-up blocker.</b></span></p>
<p><span class="txt">To continue, configure your web browser.<br> To view the instruction, click <b><i>View instruction</i></b>.<br> When you are done, click <b><i>OK</i></b> and restart the application.</span></p>
<div align="center" style="width:100%;height:50px;padding-top:20px;">
    <div style="width:450px;height:30px;position:relative;">
    <button id="okButton" class="webButton" style="left:10px;">View instruction</button>
    <button id="cancelButton" class="webButton" style="left:230px;">OK</button>
    </div>
    </div>
</div>
</div>
<!--@remove+@-->
<script type="text/javascript">
    var CLOSURE_NO_DEPS = true;
    var CLOSURE_BASE_PATH = "scripts/";
</script>
<script type="text/javascript" src="scripts/webtools/libs/closure-library/closure/goog/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/deps.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/dbgstart_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
<!--@remove-@-->
<script type="text/javascript" src="scripts/mod_browsersettingsinfomain_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
</body>
</html>
����������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>

<p>In der linken oberen Ecke finden Sie den Men&uuml;eintrag <b><i>Safari</i></b> und klicken Sie darauf. In dem ge&ouml;ffneten Fenster entfernen Sie das H&auml;kchen <b><i>Block Pop-Up Windows</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_sf_en.png?sysver=<%V8VER%>" /></div>

<p>Die Einstellung ist somit abgeschlossen.</p>

</div>
</div>

</body>
</html>
�  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Browser einrichten</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Einrichtung des Web-Browsers f&uuml;r 1C:Enterprise</p>

<p>In der linken oberen Ecke finden Sie den Men&uuml;eintrag <b><i>Safari</i></b> und klicken Sie darauf. In dem ge&ouml;ffneten Fenster entfernen Sie das H&auml;kchen <b><i>Pop-Ups unterdr&uuml;cken</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_sf_ru.png?sysver=<%V8VER%>" /></div>

<p>Die Einstellung ist somit abgeschlossen.</p>

</div>
</div>

</body>
</html>
�� ��� �#��<�\5�X�<���u�����o3t���^F � � _ � z � ��d?_E2��
���N	�D��~�G`�Y5
V
���r
�	�
z
�0	�$
�
v	��v���	�	0I�	�d
	Z�����	�
��	��
���
�9Ll%E��  ]�wj$Y=��w��(Gj���*A}`�C&���� `"  `q  ` `� `� ` `� `� `< `� `L `= `� `w `G `� `L `� `�) `P% `O `�' `� `� `U `e `�  `� `� ` `1 `�
 `� `� `� `S `S `�  `% `� ` `! `�  `	 `� `� `� `+ `h! `K	 `� `� `� ` `E `�  `z `  `[ `j `U `l `�  `{  `� `} `g `, `)  `� `�	 `� `� `� `� ` `g  `g  `M `@ `�  `f `� `
  `T  `m `B  `? `� `m `� `� `�  `� `N `] `�  `  `� `L `  `  `- `� `D `�  `K  `� `�  `� `� `0 `� `� `  `� `G
 `� ` `�	 ` `
 `� `n `L# `9  `) `1  `]  `� ` `' ` `� `k `�
 `� `� `/ `$ `� `� `� `� `j `� `= `7 `c K C  [# & �( 7+ �, 